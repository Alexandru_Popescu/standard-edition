package org.example.OCP17.Lectia14EncapsulationAndInheritance.Encapsulation;

public class Cat {

    private String name;
    private boolean dead;


    public String getName(){

        return this.name;
    }

    public void setName(String name){
        if ( !name.isBlank()){
            this.name = name;
        }else {
            System.out.println("Wrong type");
        }
    }


    public boolean isDead() {
        return dead;
    }

    public void setDead(boolean dead) {
        this.dead = dead;
    }
}
