package org.example.OCP17.Lectia14EncapsulationAndInheritance.Inheritance.ex1;

public class Ex1 {
    public static void main(String[] args) {

        A a1 = new A();
        a1.x=10;
        a1.m();

        B b1 = new B();
        b1.x=20;
        b1.m();
        System.out.println(a1.x);
        System.out.println(b1.x);
    }
}
