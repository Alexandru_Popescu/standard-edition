package org.example.OCP17.FactoryMethod;

public interface Engine {
    void run();
}
