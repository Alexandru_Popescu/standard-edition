package org.example.OCP17.FactoryMethod;

public class DieselEngine implements Engine{

    @Override
    public void run() {
        System.out.println("Diesel");
    }
}
