package org.example.OCP17.FactoryMethod;

public class ElectricEngine implements Engine{

    @Override
    public void run() {
        System.out.println("Electric");
    }
}
