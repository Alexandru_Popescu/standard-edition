package org.example.OCP17.FactoryMethod;

public final class FactoryMethod {

    private FactoryMethod(){}

    public static Engine buid(String type){
      return switch (type){
            case "Electric" ->new ElectricEngine();
            case "Diesel" -> new DieselEngine();
            default -> throw new IllegalArgumentException();

        };


    }
    int x = 013;
    int y = 0xaF43;
    int z = 0b11;




}
