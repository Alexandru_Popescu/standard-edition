package org.example.OCP17.Object.Equals;

import java.util.Objects;

public class Card {


    private String number; //12345


    public Card(String number) {
        this.number = number;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }


@Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (o instanceof Card) {
            Card c1 = (Card) o;
            return c1.getNumber().equals(this.number);


        } else {
            return false;
        }

    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }




}