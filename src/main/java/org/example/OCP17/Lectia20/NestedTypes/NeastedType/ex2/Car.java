package org.example.OCP17.Lectia20.NestedTypes.NeastedType.ex2;

public class Car {

    private int x;   // repreziinta pozitia masinii iar pozitia masinii este schmbata de engine


    class Engine {

        public void run() {

            Car.this.x++;
        }

    }

    public int getX() {
        return x;
    }
}
