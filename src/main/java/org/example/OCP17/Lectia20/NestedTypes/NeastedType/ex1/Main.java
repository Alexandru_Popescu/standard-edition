package org.example.OCP17.Lectia20.NestedTypes.NeastedType.ex1;

public class Main {
    public static void main(String[] args) {
        A.A3 a3 = new A.A3();
        A.A3.n();

        A a = new A();
        A.A2 a2 = a.new A2();

    }
}
