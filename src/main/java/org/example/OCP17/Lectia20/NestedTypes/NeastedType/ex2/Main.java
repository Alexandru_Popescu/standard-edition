package org.example.OCP17.Lectia20.NestedTypes.NeastedType.ex2;

public class Main {
    public static void main(String[] args) {

        Car c = new Car();

        Car.Engine engine = c.new Engine();
        engine.run();
        engine.run();
        engine.run();
        engine.run();
        System.out.println(c.getX());


    }

}
