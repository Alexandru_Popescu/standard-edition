package org.example.OCP17.Lectia20.NestedTypes.NeastedType.ex1;

public class A {
    int x;
    static int y;

    class A2 {

        void m() {
            System.out.println(x = 34);
        }

    }


    static class A3 {
        static void n() {
            System.out.println(y = 12);
        }
    }


}
