package org.example.OCP17.Lectia20.NestedTypes.NeastedType.ex6;

public class Main {
    public static void main(String[] args) {


        Animal animal = new Animal();
        animal.saySomething();

        Animal cat = new Animal() {
            public void saySomething() {
                System.out.println("It's a cat");
            }

        };
        cat.saySomething();

    }
}
