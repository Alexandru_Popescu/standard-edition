package org.example.OCP17.Lectia20.NestedTypes.NeastedType.ex7;

public class A {
    private int x;

    private static int y;

    class B {


        void m() {
            A.this.x = 10;
        }

    }

    static class C {

        void n() {
            A.y = 19;
        }


    }


}


