package org.example.OCP17.Lectia20.NestedTypes.NeastedType.LocalType;

public class B1 {

    void m1() {
        class B2 {
            public void sum() {
                System.out.println("Hello");
            }
        }
        B2 b2 = new B2();
        b2.sum();
    }

    static void m2() {

        class B3 {

           public static void mm() {
                System.out.println("B3");
            }
        }
        B3.mm();

    }

}
