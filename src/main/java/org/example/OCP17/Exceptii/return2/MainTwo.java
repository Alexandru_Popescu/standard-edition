package org.example.OCP17.Exceptii.return2;

public class MainTwo {
    public static void main(String[] args) {
        int x = m();
        System.out.println(x);

    }

    public static int m() {
        try {
            return 10;
        } finally {
         return    20;
        }
    }
}
