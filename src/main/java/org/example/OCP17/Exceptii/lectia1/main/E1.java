package org.example.OCP17.Exceptii.lectia1.main;

import org.example.OCP17.Exceptii.lectia1.ex1.MyCheckedException;

public interface E1 {
    void m() throws MyCheckedException;

}
