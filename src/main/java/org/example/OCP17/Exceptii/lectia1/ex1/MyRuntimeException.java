package org.example.OCP17.Exceptii.lectia1.ex1;

public class MyRuntimeException extends RuntimeException {

    public MyRuntimeException() {
        super("This is my runtime exception");
    }

}
