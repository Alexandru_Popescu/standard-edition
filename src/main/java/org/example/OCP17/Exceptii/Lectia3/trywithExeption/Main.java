package org.example.OCP17.Exceptii.Lectia3.trywithExeption;

public class Main {

    public static void main(String[] args) {

        try (Re r = new Re()) {

            throw new RuntimeException();
        } catch (Exception r) {
            System.out.println(r);
            Throwable[] t = r.getSuppressed();
            for (Throwable throwable : t) {
                System.out.println(throwable);
            }
        }


    }
}
