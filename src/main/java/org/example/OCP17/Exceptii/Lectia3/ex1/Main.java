package org.example.OCP17.Exceptii.Lectia3.ex1;

public class Main {
    public static void main(String[] args)  {

        R r = null;

        try {
            //r
            r = new R();
        } catch (Exception e) {
//r
        } finally {
          if (r==null)
              r.close();
        }

    }
}
