package org.example.OCP17.Exceptii.tryoutside;

import org.example.OCP17.Exceptii.Lectia3.trywithExeption.Re;

public class Main {
    public static void main(String[] args) {


        Re re = new Re();
        Re re2 = new Re();


        try (re;re2) {

        } catch (Exception e) {
        }


//        re.close();

    }
}
