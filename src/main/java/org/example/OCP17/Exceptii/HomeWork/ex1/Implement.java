package org.example.OCP17.Exceptii.HomeWork.ex1;

public class Implement {
    private final static int value = 100;


    public void check(int value) throws Exceptions {

        if (value > 100) {
            throw new Exceptions();
        } else {
            System.out.println("PLease wait for your money");
        }

    }

}