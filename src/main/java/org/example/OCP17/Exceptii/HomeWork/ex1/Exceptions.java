package org.example.OCP17.Exceptii.HomeWork.ex1;

public class Exceptions extends RuntimeException {

    public Exceptions() {
        super("You don't have enough money in your bank account ");
    }

}
