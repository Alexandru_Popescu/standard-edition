package org.example.OCP17.Lectia17.PolymorphismAndAbstractClasses.Polymorphism.ex6.Recap_Casting;

public class Main {
    public static void main(String[] args) {


        Animal a1 = new Dog();
        Animal a2 = new Cat();   // no cast
        Cat c1 = (Cat) a2;     //more particular -> cast
//        Cat c2 = (Cat) a1;     // classCastException
//        Cat c3 = (Cat) new Dog();  // compilation error


    }
}