package org.example.OCP17.Lectia17.PolymorphismAndAbstractClasses.Abstraction.ex1;

public class Circle extends Shape {
    double radius;

    public double area() {
        return 3.14 * radius * radius;
    }

}
