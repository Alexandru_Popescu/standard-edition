package org.example.OCP17.Lectia17.PolymorphismAndAbstractClasses.Polymorphism.ex4;

public class Dog extends Animal{
    @Override
    void m() {
        System.out.println("This is an Dog");
    }

    void sayDog(){
        System.out.println("Bark");

    }

}
