package org.example.OCP17.Lectia17.PolymorphismAndAbstractClasses.Polymorphism.ex2;

public class Main {
    public static void main(String[] args) {

        A a1 = new B();
        B b1 = (B) a1;
        a1.m();
        a1.x = 20;
        ((B) a1).n();
//        a1.m();
//        b1.m();

//

        //  A a2 = new A();
        //  B b2 = (B) a2;
    }
}
