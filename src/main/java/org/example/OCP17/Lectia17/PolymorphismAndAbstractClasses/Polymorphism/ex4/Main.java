package org.example.OCP17.Lectia17.PolymorphismAndAbstractClasses.Polymorphism.ex4;

public class Main {
    public static void main(String[] args) {

        Cat cat = new Cat();
        cat.sayCat();
        check(cat);


    }

    public static void check(Animal animal) {

       if (animal instanceof Dog) {
           animal.m();
           Dog myDog = (Dog) animal;
           myDog.sayDog();
       }


    }


}

