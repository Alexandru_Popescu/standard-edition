package org.example.OCP17.Lectia17.PolymorphismAndAbstractClasses.Polymorphism.ex3;

public class Main {
    public static void main(String[] args) {

        Animal a1 = new Animal();
        Animal a2 = new Dog();
        Animal a3 = new Cat();
        Cat c4 = (Cat) new Animal();
        Animal a4 = a2;
        Dog d5 = (Dog) a2;
//        Cat c1 = (Cat) a2;

//        Cat c2 = new Dog();
//        Cat c3 = (Cat) new Dog();

    }
}
