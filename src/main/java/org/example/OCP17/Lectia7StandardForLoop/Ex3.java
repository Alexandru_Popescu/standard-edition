package org.example.OCP17.Lectia7StandardForLoop;

public class Ex3 {
    public static void main(String[] args) {


        for (var x = 10; x >=1; x--) {
            System.out.println(x);
        }

    }
}
