package org.example.OCP17.Lectia7StandardForLoop;

public class Ex5 {
    public static void main(String[] args) {
        var x = 10;

        for (; ; ) {
            System.out.println(x);
            x--;
            if (x ==0){
                break;
            }
        }

    }
}
