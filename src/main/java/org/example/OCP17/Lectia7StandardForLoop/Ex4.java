package org.example.OCP17.Lectia7StandardForLoop;

public class Ex4 {
    public static void main(String[] args) {
        var x = 10;

        for (; x >=1;) {
            System.out.println(x);
            x--;
        }

    }
}
