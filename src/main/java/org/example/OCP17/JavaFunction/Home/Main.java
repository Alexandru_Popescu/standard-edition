package org.example.OCP17.JavaFunction.Home;

import java.util.Map;
import java.util.function.BiConsumer;

public class Main {
    public static void main(String[] args) {

        Map<Integer, String> m = Map.of(
                1, "a",
                2, "b",
                3, "c"
        );


        m.forEach((x, y) -> System.out.println(x + " " + y));
        BiConsumer<Integer, String> b = (z, c) -> System.out.println(z + "" + c);
        m.forEach(b);

    }
}
