package org.example.OCP17.JavaFunction.Consumer.ex3;

import java.util.List;
import java.util.function.Consumer;

public class Main {
    public static void main(String[] args) {

        List<Integer> l1 = List.of(1, 2, 34, 543);

        Consumer<Integer> c1 = (c) -> System.out.println(c);
        l1.forEach(c1);

    }
}
