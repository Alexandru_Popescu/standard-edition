package org.example.OCP17.JavaFunction.Consumer.ex2;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class Main {
    public static void main(String[] args) {

        Consumer<Integer> c1 = (s) -> System.out.println(s);
        c1.accept(34);

        BiConsumer<String,Integer> b1 = (a,b) -> System.out.println(a +  " " + b);
        b1.accept("Ceau", 31);



    }
}
