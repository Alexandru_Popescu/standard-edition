package org.example.OCP17.JavaFunction.Consumer.ex4;

import java.util.Map;
import java.util.function.BiConsumer;

public class Main {
    public static void main(String[] args) {

        Map<Integer, String> m1 = Map.of(
                2, "a",
                3, "b");
        m1.forEach((x, y) -> System.out.println(x + " " + y));
        BiConsumer<Integer,String> b1 = (a,b) -> System.out.println(a + " " + b);
        m1.forEach(b1);

    }

}
