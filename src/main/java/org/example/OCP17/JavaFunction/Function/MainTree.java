package org.example.OCP17.JavaFunction.Function;

import java.util.function.BiFunction;
import java.util.function.Function;

public class MainTree {
    public static void main(String[] args) {

        Function<String,Integer> f = s -> s.length();
        int res = f.apply("Alex");
        System.out.println(res);
        BiFunction<Integer, Integer,Integer> b1 = (a,b) -> (a + b);
        int res1 = b1.apply(43 ,2);
        System.out.println(res1);


    }
}
