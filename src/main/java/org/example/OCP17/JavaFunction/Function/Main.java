package org.example.OCP17.JavaFunction.Function;

import java.util.function.BiFunction;
import java.util.function.Function;

public class Main {
    public static void main(String[] args) {


        Function<String, Integer> f1 = s -> s.length();

        int x = f1.apply("Hello");
        System.out.println(x);


        BiFunction<Integer, Integer, String> b1 = (s1, c1) -> s1  + "" + c1;
        String res = b1.apply(3 ,5 );
        System.out.println(res);







    }
}
