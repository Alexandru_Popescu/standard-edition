package org.example.OCP17.JavaFunction.Function;

import java.util.function.BiFunction;

public class Main2 {
    public static void main(String[] args) {


        BiFunction<Integer, Integer, Integer> b = (x, y) -> (x +  y);
        int res = b.apply(3, 5);
        System.out.println(res);


    }
}