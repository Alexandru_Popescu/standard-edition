package org.example.OCP17.JavaFunction.Binary;

import java.util.function.BinaryOperator;
import java.util.function.DoubleBinaryOperator;
import java.util.function.LongPredicate;

public class Main {
    public static void main(String[] args) {


        BinaryOperator<Integer> b1 = (a,b) -> a + b;


        DoubleBinaryOperator b2 = (x, y) -> x + y ;
        double x = b2.applyAsDouble(3,5);
        System.out.println(x);

        LongPredicate l = z -> z < 5;
      boolean  w=  l.test(45);
        System.out.println(w);


    }
}