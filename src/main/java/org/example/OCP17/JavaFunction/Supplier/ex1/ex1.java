package org.example.OCP17.JavaFunction.Supplier.ex1;

import java.util.Random;
import java.util.function.Supplier;

public class ex1 {
    public static void main(String[] args) {

        Random r = new Random();
        Supplier<Integer> s1 = () -> r.nextInt(100);
        System.out.println(s1.get());

    }
}
