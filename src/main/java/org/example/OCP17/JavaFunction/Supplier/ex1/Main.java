package org.example.OCP17.JavaFunction.Supplier.ex1;

import java.util.function.Supplier;

public class Main {
    public static void main(String[] args) {

        Supplier<Integer> s1 = () -> 5;
        System.out.println(s1.get());
    }
}
