package org.example.OCP17.JavaFunction.Predicate;

import java.util.function.BiPredicate;

public class Ex1 {
    public static void main(String[] args) {

        BiPredicate<String, Integer> b1 = (s, x) -> s.length() == x;
        boolean a = b1.test("Alex", 9);
        boolean b = b1.test("Alex", 5);
        boolean c = b1.test("Alex", 4);
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);


    }
}
