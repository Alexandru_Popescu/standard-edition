package org.example.OCP17.JavaFunction.Predicate;

import java.util.function.BiPredicate;
import java.util.function.Predicate;

public class Main {
    public static void main(String[] args) {

        Predicate<Integer> p1 = s -> s % 2 == 0;
        boolean a = p1.test(43);
        boolean b = p1.test(44);
        boolean c = p1.test(47);
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);

        BiPredicate<Integer, Integer> b1 = (x, y) -> x > y;
        boolean d = b1.test(4, 6);
        boolean e = b1.test(7, 1);
        boolean f = b1.test(87, 6);
        System.out.println(d);
        System.out.println(e);
        System.out.println(f);

    }
}
