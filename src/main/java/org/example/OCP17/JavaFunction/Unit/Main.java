package org.example.OCP17.JavaFunction.Unit;

import java.util.function.UnaryOperator;

public class Main {

    public static void main(String[] args) {
        UnaryOperator<Integer> u1 = x -> x +3;
        int y = u1.apply(4);
        System.out.println(y);

       UnaryOperator<String> u2 = x -> new StringBuilder(x).reverse().toString();
       String text = u2.apply("Alex");
        System.out.println(text);

    }
}
