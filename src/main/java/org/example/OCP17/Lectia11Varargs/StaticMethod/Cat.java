package org.example.OCP17.Lectia11Varargs.StaticMethod;

public class Cat {

    String name;
    static void SayMeow() {
        System.out.println("Meow! My name is: "  );
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
