package org.example.OCP17.Lectia11Varargs.Varags;

public class Example2 {
    public static void main(String[] args) {

        m();
        m(1);
        m(99, 3, 4, 5, 43, 342, 2, 3);

    }

    static void m(int... a) {
        for (int y :a){
            System.out.print(y);
        }
        System.out.println();

    }


}
