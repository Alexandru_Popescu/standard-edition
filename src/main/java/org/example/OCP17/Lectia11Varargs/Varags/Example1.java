package org.example.OCP17.Lectia11Varargs.Varags;

public class Example1 {
    public static void main(String[] args) {

        m();
        m(1);
        m(99, 3, 4, 5, 43, 342, 2, 3);

    }

    static void m(int... a) {
        int result = a.length;
        if (result > 0) {
            System.out.println(result);
        } else {
            System.out.println("No data there");
        }
    }


}
