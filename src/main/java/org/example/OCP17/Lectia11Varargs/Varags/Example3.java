package org.example.OCP17.Lectia11Varargs.Varags;

public class Example3 {
    public static void main(String[] args) {

        int[] x = {1, 2, 3};
        int[] y = {1, 2, 3};
        int[][] c = {x, y};
        m(x, y);
        m(c);
        m();
        m(new int[]{1, 2,5,});

    }
    static void m(int[]... a) {
        for (int[] b : a) {
            for (int c : b) {
                System.out.print(c + " ");
            }
            System.out.println();


        }


    }

}
