package org.example.OCP17.Lectia11Varargs.Varags;

public class ExampleTwo {
    public static void main(String[] args) {

        m();
        m(1, 2, 3, 4, 5);
        m(2, 4);
        System.out.println("=============");
        m2("ABC",1,2,3);
        System.out.println("=============");
        m3(new int[]{2,3,4,5});

    }

    static void m(int... a) {
        int x = a.length;
        if (x > 0) {

            System.out.println(a[0]);
        } else {
            System.out.println("Warning");
        }
    }

    static void m2(String a, int...b){
        System.out.println(a);
        System.out.println(b.length);
    }

    static void m3(int[]... c){
        for (int[] ints : c) {
            for (int j : ints){
                System.out.println(j);
            }
            System.out.println();
        }
    }

}
