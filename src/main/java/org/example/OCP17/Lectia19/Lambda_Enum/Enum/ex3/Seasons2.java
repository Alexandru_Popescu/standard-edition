package org.example.OCP17.Lectia19.Lambda_Enum.Enum.ex3;

public enum Seasons2 {
    SPRING {
        @Override
        void m() {

        }
    },
    SUMMER {
        @Override
        void m() {

        }
    },
    AUTUMN {
        @Override
        void m() {

        }
    },
    WINTER {
        @Override
        void m() {

        }
    };


    abstract void m();

}
