package org.example.OCP17.Lectia19.Lambda_Enum.Enum.Ex1;

//public enum Coffee {
//    Small,Medium, Big;
//
//
//
//}

public final class Coffee {

    public final static Coffee Small = new Coffee();
    public final static Coffee Medium = new Coffee();
    public final static Coffee Big = new Coffee();

    private Coffee() {
    }
}
