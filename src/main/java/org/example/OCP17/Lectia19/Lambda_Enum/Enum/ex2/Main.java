package org.example.OCP17.Lectia19.Lambda_Enum.Enum.ex2;

public class Main {

    public static void main(String[] args) {

        Coffee coffee = Coffee.MEDIUM;
        coffee.setQty(4);
        System.out.println(coffee.getQty());
        Coffee coffee2 = Coffee.SMALL;
        System.out.println(coffee2.getQty());

        switch (coffee) {
            case SMALL:
                System.out.println("OK");
                break;
            case MEDIUM:
                System.out.println("Good");
                break;

        }

        int a = Coffee.SMALL.ordinal();
        System.out.println(a);
        String name = Coffee.MEDIUM.name();
        System.out.println(name);

        for (Coffee coffeeParse : Coffee.values()) {
            System.out.println(coffeeParse.name());
        }


    }

}


