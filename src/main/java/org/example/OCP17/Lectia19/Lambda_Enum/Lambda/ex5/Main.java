package org.example.OCP17.Lectia19.Lambda_Enum.Lambda.ex5;

import java.util.function.Function;

public class Main {
    public static void main(String[] args) {

        // x -> y encoding "ABCD" -> DCBA

        Reverse r1 = (x) -> new StringBuilder(x).reverse().toString();
        String y = r1.reverse("ABC");
        System.out.println(y);

        Function<String, String> f1 = (x) -> new StringBuilder(x).reverse().toString();
        String z = f1.apply("ABC");
        System.out.println(z);


    }
}