package org.example.OCP17.Lectia19.Lambda_Enum.Enum.ex2;

public enum Coffee {
    SMALL(5), MEDIUM(), BIG;    //first in the Enum

    private int qty;



    Coffee (){
        System.out.println(":)");
    }

     Coffee(int qty){
        this.qty=qty;
    }


    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }
}


//public final class Coffee {
//    public final static Coffee SMALL = new Coffee();
//    public final static Coffee MEDIUM = new Coffee();
//    public final static Coffee BIG = new Coffee();
//
//    Coffee() {
//    }


