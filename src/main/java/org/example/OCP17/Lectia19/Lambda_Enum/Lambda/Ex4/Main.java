package org.example.OCP17.Lectia19.Lambda_Enum.Lambda.Ex4;

public class Main {

    public static void main(String[] args) {

        Z z1 = () -> 5;
        Z z2 = () -> n();
        Z z3 = () -> {
            int e = 3;
            return e;
        };

        Z z4 = () -> n();
        int x = n();
        System.out.println(x);


    }

    static int n() {
        return 4;

    }

}
