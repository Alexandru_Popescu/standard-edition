package org.example.OCP17.Lectia19.Lambda_Enum.Lambda.ex5;

public interface Reverse {

    String reverse(String input);

}
