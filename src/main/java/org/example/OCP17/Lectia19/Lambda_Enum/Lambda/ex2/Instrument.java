package org.example.OCP17.Lectia19.Lambda_Enum.Lambda.ex2;

@FunctionalInterface
public interface Instrument {
    void play();

    default void n() {
    }

    static void s() {
    }

}
