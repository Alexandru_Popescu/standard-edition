package org.example.OCP17.Lectia19.Lambda_Enum.Lambda.Ex3;

public class Main {
    public static void main(String[] args) {

        X x = a -> System.out.println("Hello");
        X x1 = (a) -> System.out.println("Hello");
        X x2 = (int a) -> System.out.println("Hello");
        X x3 = (var a) -> System.out.println("Hello");


        Y y = (a,b) -> System.out.println("Hi");
        Y y1 = (int a, String b) -> System.out.println("Hi");
        Y y2 = (var a, var b) -> System.out.println("Hi");

    }
}