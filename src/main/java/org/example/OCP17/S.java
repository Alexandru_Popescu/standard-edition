package org.example.OCP17;

public class S {


    private final String greeting = "Hi";

    protected class Home {

        public int repeat = 3;

        public void enter() {
            for (int i = 0; i < repeat; i++) {
                greet(greeting);
            }
        }
        private static void greet(String message) {
            System.out.println(message);
        }

    }


}
