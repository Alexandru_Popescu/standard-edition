package org.example.OCP17;

public class B {
    public static void main(String[] args) {
        String s = "\" Alexandru Popescu\"\n Cel mai tare";
        System.out.println(s);

        String y = """
                "Alexandru Popescu"
                "cel mai tare"
                                
                """;
        System.out.println(y);

        String text = "\" Alex\" \n cel mai bun";
        System.out.println(text);
    }
}
