package org.example.OCP17.Stream.Optional.Lectia1;

import java.util.Optional;

public class Ex3 {
    public static void main(String[] args) {


        Optional<Integer> op1 = Optional.empty();
        Optional<Integer> op2 = Optional.of(10);

       Integer a =  op2.get();


      if (op1.isPresent()) {
          Integer b = op1.get();
          System.out.println(b);
      }else {
          System.out.println("Empty ");
      }
        System.out.println(a);



    }
}
