package org.example.OCP17.Stream.Optional.Lectia1;

import java.util.Optional;

public class ex1 {
    public static void main(String[] args) {


        Optional<Integer> op1 = Optional.empty();
        Optional<Integer> op2 = Optional.of(10);

//        Optional<Integer> op3 = Optional.of(m());
        Optional<Integer> op4 = Optional.ofNullable(m());
        System.out.println(op2);
        System.out.println(op4);


    }


    static Integer m() {
        return null;
    }
}
