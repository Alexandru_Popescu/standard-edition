package org.example.OCP17.Stream.Optional.Lectia1;

import java.util.Optional;

public class Ex5 {
    public static void main(String[] args) {


        Optional<Integer> op1 = Optional.empty();
        Optional<Integer> op2 = Optional.of(10);


        //map
//       Integer x =op1.map(s -> s*2).orElse(-2);
//        System.out.println(x);
//        Integer y =op2.map(s -> s*2).orElse(-2);
//        System.out.println(y);

        //flat map

        var x = op1.flatMap(s -> Optional.of(2*s)).orElse(-1);
        System.out.println(x);
        var y = op2.flatMap(s -> Optional.of(2*s)).orElse(-1);
        System.out.println(y);


    }
}
