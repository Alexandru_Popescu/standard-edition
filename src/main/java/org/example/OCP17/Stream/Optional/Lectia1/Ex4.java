package org.example.OCP17.Stream.Optional.Lectia1;

import java.util.Optional;

public class Ex4 {
    public static void main(String[] args) {


        Optional<Integer> op1 = Optional.empty();
        Optional<Integer> op3 = Optional.empty();
        Optional<Integer> op2 = Optional.of(10);


        //OrElse
//     Integer res1 = op1.orElse(3);
//     Integer res2 = op2.orElse(3);
//        System.out.println(res1);
//        System.out.println(res2);

        //OrElseGet
//        Supplier<Integer> s1 = () -> 1;
//        Integer re3 = op1.orElseGet(s1);
//        System.out.println(re3);

//           or
       Integer res4 = op1.or(()->op2).get();
        System.out.println(res4);

    }
}
