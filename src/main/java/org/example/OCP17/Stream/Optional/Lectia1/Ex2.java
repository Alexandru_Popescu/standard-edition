package org.example.OCP17.Stream.Optional.Lectia1;

import java.util.Optional;

public class Ex2 {
    public static void main(String[] args) {


        Optional<Integer> op1 = Optional.empty();
        Optional<Integer> op2 = Optional.of(10);
       boolean b1= op1.isPresent();
       boolean b2= op2.isPresent();
        System.out.println(b1);
        System.out.println(b2);
    }
}
