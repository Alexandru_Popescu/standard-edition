package org.example.OCP17.Stream.Lectia2;

import java.util.Arrays;
import java.util.List;

public class Ex6 {
    public static void main(String[] args) {


        List<String> list = List.of("34fs32", "34sfd3s", "45t3y");
//10 digits


        String digits = "0123456789";

        long x = list.stream()
                .flatMap(s -> Arrays.stream(s.split("")))
                .filter(s -> digits.contains(s))
                 .count();
        System.out.println(x);

    }
}
