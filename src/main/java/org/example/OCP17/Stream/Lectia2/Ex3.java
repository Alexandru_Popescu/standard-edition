package org.example.OCP17.Stream.Lectia2;

import java.util.List;

public class Ex3 {
    public static void main(String[] args) {

        List<String> list = List.of("abc", "no", "yes", "Dan");

       int x = list.stream()
                .mapToInt(s->s.length())
                .sum();
        System.out.println(x);


    }
}
