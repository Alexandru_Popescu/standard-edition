package org.example.OCP17.Stream.Lectia2;

import java.util.List;

public class Ex4 {
    public static void main(String[] args) {

        List<List<Integer>> list = List.of(
                List.of(1, 2, 3, 4),
                List.of(5, 6, 7),
                List.of(8, 9, 10)
        );

        long x = list.stream()
                .flatMap(q -> q.stream())
                .reduce(0, (a, b) -> (a + b));
        System.out.println(x);

    }
}
