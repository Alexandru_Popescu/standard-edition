package org.example.OCP17.Stream.Lectia2;

import java.util.List;

public class Ex2 {
    public static void main(String[] args) {

        List<String> list = List.of("abc", "no", "yes");

        list.stream()
                .map(s ->new StringBuilder(s).reverse().toString())

                .forEach(System.out::println);



    }
}
