package org.example.OCP17.Stream.Lectia2;

import java.util.List;

public class Ex1 {
    public static void main(String[] args) {
        List<String> list = List.of("abc", "no", "yes");

        int x =list.stream()
                .map((s -> s.length()))
                .reduce(0, (a, b) -> (a + b));
        System.out.println(x);


    }
}
