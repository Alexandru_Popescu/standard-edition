package org.example.OCP17.Stream.Lectia2;

import java.util.List;
import java.util.stream.Collectors;

public class Ex5 {
    public static void main(String[] args) {

        List<String> s1 = List.of("ALEX", "MIHAI", "IONUT", "LISU");

//        List<String> text = s1.stream()
//                .map(w -> w.toLowerCase())
//                .collect(Collectors.toList());
////        System.out.println(text);


        List<String> s2 = List.of("ALEX", "MIHAI", "IONUT", "LISU");
        List<Character> text1 = s2.stream()
                .flatMap(w -> w.chars().mapToObj(c -> (char) c))
                .collect(Collectors.toList());
        System.out.println(text1);

//        s2.stream()
//                .flatMap(x -> Arrays.stream(x.split("")))
//                .forEach(System.out::println);



    }
}
