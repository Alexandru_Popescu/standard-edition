package org.example.OCP17.Stream.Collector.lectia1;

import java.util.List;
import java.util.stream.Collectors;

public class Ex12 {
    public static void main(String[] args) {


        List<String> list = List.of("A", "AA", "B", "BB", "CCC", "DDD");

var x =
        list.stream()
                .collect(
                        Collectors.mapping(
                                s -> s.length(),
                                Collectors.counting()

                        ));
        System.out.println(x);

    }
}
