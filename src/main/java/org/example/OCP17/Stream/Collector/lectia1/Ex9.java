package org.example.OCP17.Stream.Collector.lectia1;

import java.util.List;
import java.util.stream.Collectors;

public class Ex9 {

    public static void main(String[] args) {

        List<String> list = List.of("AAA", "B", "CCCC", "DDD", "FFFFFF", "AAA");


        List<Integer> myList = list.stream()
                .collect(
                        Collectors.mapping(
                        s -> s.length(),
                        Collectors.filtering(n -> n % 2 == 0,
                                Collectors.toList())

                ));
        System.out.println(myList);

    }
}
