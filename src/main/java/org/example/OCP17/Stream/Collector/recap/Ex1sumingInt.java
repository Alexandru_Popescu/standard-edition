package org.example.OCP17.Stream.Collector.recap;


import java.util.List;
import java.util.stream.Collectors;

public class Ex1sumingInt {
    public static void main(String[] args) {

        List<String> list = List.of("AB", "CCC", "DDDDD", "E", "FFFFFFF");


        int x =
                list.stream()
                        .mapToInt(s -> s.length())
                        .sum();
        System.out.println(x);


        int y =
                list.stream()
                        .collect(Collectors.summingInt(s -> s.length()));
        System.out.println(y);

    }
}
