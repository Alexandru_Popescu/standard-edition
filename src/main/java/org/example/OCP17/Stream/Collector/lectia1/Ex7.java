package org.example.OCP17.Stream.Collector.lectia1;

import java.util.List;
import java.util.stream.Collectors;

public class Ex7 {
    public static void main(String[] args) {

        List<String> list = List.of("AAA", "B", "CCCC", "DDD", "FFFFF", "AAA");


        var res = list.stream()
                .collect(Collectors.teeing(
                        Collectors.counting(),
                        Collectors.joining(),
                        (a, b) -> List.of(a, b)


                ));
        System.out.println(res);

    }
}
