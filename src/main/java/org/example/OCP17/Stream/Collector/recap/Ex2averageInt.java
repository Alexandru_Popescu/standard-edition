package org.example.OCP17.Stream.Collector.recap;


import java.util.List;
import java.util.OptionalDouble;
import java.util.stream.Collectors;

public class Ex2averageInt {
    public static void main(String[] args) {

        List<String> list = List.of("AB", "CCC", "DDDDD", "E", "FFFFFFF");


       OptionalDouble x =
list.stream()
        .mapToDouble(s->s.length())
        .average();

        System.out.println(x);

        double y =
        list.stream()
                .collect(Collectors.averagingInt(s->s.length()));
        System.out.println(y);
    }
}
