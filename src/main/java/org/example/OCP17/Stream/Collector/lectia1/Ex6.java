package org.example.OCP17.Stream.Collector.lectia1;

import java.util.List;
import java.util.stream.Collectors;

public class Ex6 {
    public static void main(String[] args) {

        List<String> list = List.of("AAA", "B", "CCCC", "DDD", "FFFFF", "AAA");

        String m1 =

                list.stream()
                        .collect(Collectors.joining(",","1","z"));
        System.out.println(m1);


    }
}
