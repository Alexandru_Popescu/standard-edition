package org.example.OCP17.Stream.Collector.Lectia2;

import java.util.List;
import java.util.stream.Collectors;

public class Ex3 {
    public static void main(String[] args) {

        List<String> list = List.of("AAA", "BB", "C", "DDDDDD", "E");


       double x =
                list.stream()
                        .collect(Collectors.averagingInt(s -> s.length()));
        System.out.println(x);

    }
}