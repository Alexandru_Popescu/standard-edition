package org.example.OCP17.Stream.Collector.Lectia2;

import java.util.List;
import java.util.stream.Collectors;

public class Ex1 {
    public static void main(String[] args) {

        List<String> list = List.of("AAA", "BB", "C", "DDDDDD", "E");

        var x =
                list.stream()
                        .mapToInt(s -> s.length())
                        .sum();

        System.out.println(x);

        var y =
                list.stream()
                        .collect(Collectors.summingInt(s -> s.length()));

        System.out.println(y);
    }


}
