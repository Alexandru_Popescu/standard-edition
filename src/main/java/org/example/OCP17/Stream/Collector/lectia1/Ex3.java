package org.example.OCP17.Stream.Collector.lectia1;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Ex3 {
    public static void main(String[] args) {

        List<String> list = List.of("AAA", "B", "CCCC", "DDD", "FFFFF", "AAA");

        Map<String, Integer> map1 =

                list.stream()
                        .collect(Collectors.toMap(
                                s -> s,
                                s -> s.length(),
                                (a, b) -> a + b));

        System.out.println(map1);


    }
}
