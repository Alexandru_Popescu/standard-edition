package org.example.OCP17.Stream.Collector.lectia1;

import java.util.List;
import java.util.stream.Collectors;

public class Ex8 {

    public static void main(String[] args) {

        List<String> list = List.of("AAA", "B", "CCCC", "DDD", "FFFFF", "AAA");


        List<Integer> myList = list.stream()
                .collect(Collectors.mapping(
                        a -> a.length(),
                        Collectors.toList()

                ));
        System.out.println(myList);

    }
}
