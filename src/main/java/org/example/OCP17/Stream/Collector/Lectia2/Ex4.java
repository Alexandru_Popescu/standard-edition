package org.example.OCP17.Stream.Collector.Lectia2;

import java.util.IntSummaryStatistics;
import java.util.List;

public class Ex4 {
    public static void main(String[] args) {

        List<String> list = List.of("AAA", "BB", "C", "DDDDDD", "E");
        var x =
                list.stream()
                        .mapToInt(s -> s.length())
                        .sum();
        System.out.println(x);

        var y =
                list.stream()
                        .mapToInt(s -> s.length())
                        .average();
        System.out.println(y);


       IntSummaryStatistics res =
        list.stream()
                .mapToInt(s->s.length())
                .summaryStatistics();
        System.out.println(res);
    }
}