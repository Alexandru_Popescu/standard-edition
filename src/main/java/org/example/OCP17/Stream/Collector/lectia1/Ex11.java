package org.example.OCP17.Stream.Collector.lectia1;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Ex11 {

    public static void main(String[] args) {

        List<Integer> list = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);


        Map<Boolean, List<Integer>> myList = list.stream()
                .collect(Collectors.partitioningBy(s -> s >= 7));
        System.out.println(myList);

    }
}
