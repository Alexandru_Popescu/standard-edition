package org.example.OCP17.Stream.Collector.lectia1;

import java.util.List;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class Ex5 {
    public static void main(String[] args) {

        List<String> list = List.of("AAA", "B", "CCCC", "DDD", "FFFFF", "AAA", "AAA");

        TreeMap<String, Integer> m1 =

                list.stream()
                        .collect(
                                Collectors.toMap(
                                        s -> s,
                                        s -> s.length(),
                                        (a, b) -> a + b,
                                        () ->new TreeMap<>()
                                ));


        System.out.println(m1);


    }
}
