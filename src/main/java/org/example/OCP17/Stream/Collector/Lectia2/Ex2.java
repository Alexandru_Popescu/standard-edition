package org.example.OCP17.Stream.Collector.Lectia2;

import java.util.List;
import java.util.stream.Collectors;

public class Ex2 {
    public static void main(String[] args) {

        List<String> list = List.of("AAA", "BB", "C", "DDDDDD", "E");


      var x =

                list.stream()
                        .collect(Collectors.mapping(s -> s.length(),
                                Collectors.counting()));
//                                Collectors.toList()));
        System.out.println(x);
    }
}