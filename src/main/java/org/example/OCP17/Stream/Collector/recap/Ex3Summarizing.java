package org.example.OCP17.Stream.Collector.recap;


import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.stream.Collectors;

public class Ex3Summarizing {
    public static void main(String[] args) {

        List<String> list = List.of("AB", "CCC", "DDDDD", "E", "FFFFFFF");

        IntSummaryStatistics x =
                list.stream()
                        .mapToInt(s -> s.length())
                        .summaryStatistics();
        System.out.println(x);

        IntSummaryStatistics y =
                list.stream()
                        .collect(Collectors.summarizingInt(s -> s.length()));
        System.out.println(y);
    }
}
