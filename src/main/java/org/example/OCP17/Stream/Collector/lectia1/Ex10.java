package org.example.OCP17.Stream.Collector.lectia1;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Ex10 {

    public static void main(String[] args) {

        List<String> list = List.of("AAA", "B", "CCCC", "DDD", "FFFFF", "AAA");

        Map<Boolean, List<String>>myMap =
        list.stream()
                .collect(Collectors.partitioningBy(s -> s.length() % 2 == 0));

        System.out.println(myMap);
    }
}
