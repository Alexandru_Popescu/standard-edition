package org.example.OCP17.Stream.Collector.lectia1;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class Ex1 {
    public static void main(String[] args) {

        List<String> list = List.of("AAA", "B", "CCCC", "DDD", "FFFFF");


        List<String> res1 =
                list.stream()
                        .collect(Collectors.toList());
        System.out.println(res1);

        Set<String> res2 =
                list.stream()
                        .collect(Collectors.toSet());
        System.out.println(res2);



        TreeSet<String> res3 =
                list.stream()
                                .collect(Collectors.toCollection(TreeSet::new));
        System.out.println(res2);



    }




}
