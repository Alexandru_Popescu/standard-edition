package org.example.OCP17.Stream.Lectia3;

import java.util.List;

public class Ex5 {
    public static void main(String[] args) {

        List<Integer> list = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10,12);

        list.stream()
                .filter(i -> i % 2 == 0)
                .skip(4)
                .forEach(System.out::println);


    }
}
