package org.example.OCP17.Stream.Lectia3;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Ex2 {

    public static void main(String[] args) {


        List<Integer> list = List.of(1, 3, 4, 5, 2, 6, 2, 54, 54, 34);
        Comparator<Integer> c = Collections.reverseOrder();

        list.stream()
                .distinct()
                .sorted(Collections.reverseOrder())
                .forEach(System.out::print);


    }
}
