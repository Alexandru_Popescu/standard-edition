package org.example.OCP17.Stream.Lectia3;

import java.util.List;

public class Ex1 {

    public static void main(String[] args) {


        List<Integer> list = List.of(1, 3, 4, 5, 2, 6,2, 54,54,34);

        list.stream()
                .distinct()
                .sorted()
                .forEach(System.out::print);




    }
}
