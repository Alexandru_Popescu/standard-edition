package org.example.OCP17.Stream.Lectia3;

import java.util.stream.Stream;

public class Ex3 {
    public static void main(String[] args) {

        Stream<Cat> myCat = Stream.of(

                new Cat(4),
                new Cat(2),
                new Cat(3),
                new Cat(8)

        );

                myCat.sorted()
                .forEach(c -> System.out.println(c.getAge()));


    }
}
