package org.example.OCP17.Stream.Lectia3;

import java.util.ArrayList;
import java.util.List;

public class Ex7 {
    public static void main(String[] args) {

        List<Integer> input = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        List<Integer> output = new ArrayList<>();     // 2,4,6,8,10


        input.stream()   // 1, 2, 3, 4, 5, 6, 7, 8, 9, 10
                .filter(n -> n % 2 == 0) // 2, 4, 6, 8,10
                .peek(n -> output.add(n))
                .forEach(System.out::println);

        System.out.println(output);

    }

}
