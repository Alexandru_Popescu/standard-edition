package org.example.OCP17.Stream.Lectia3;

import java.util.List;

public class Ex8 {
    public static void main(String[] args) {

        List<Integer> list = List.of(1, 2, 3, 450, 70, 100, 130);

        list.stream()
                .takeWhile(s -> s <= 100)
                .forEach(System.out::println);


    }
}
