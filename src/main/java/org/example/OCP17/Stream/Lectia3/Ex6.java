package org.example.OCP17.Stream.Lectia3;

import java.util.List;

public class Ex6 {
    public static void main(String[] args) {

        List<Integer> list = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12);

        list.stream()    // 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12
                .filter(n -> n % 2 == 0)  //     // 2,4,6,8,10, 12
                .peek(n -> System.out.println(n)) // 2,4,6,8,10, 12
                .forEach(System.out::println);
    }
}
