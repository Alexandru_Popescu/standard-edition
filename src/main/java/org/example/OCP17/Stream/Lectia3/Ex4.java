package org.example.OCP17.Stream.Lectia3;

import java.util.Comparator;
import java.util.stream.Stream;

public class Ex4 {

    public static void main(String[] args) {

        Stream<Dog> myDogs = Stream.of(

                new Dog(3),
                new Dog(5),
                new Dog(36),
                new Dog(32),
                new Dog(23)

        );

        Comparator<Dog> d1 = (a, b) -> a.getAge() - b.getAge();
        myDogs.sorted(d1)
                .forEach(System.out::println);


    }


}
