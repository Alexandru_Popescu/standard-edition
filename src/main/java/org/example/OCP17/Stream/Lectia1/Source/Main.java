package org.example.OCP17.Stream.Lectia1.Source;

import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {

//        Stream<Integer> s1 = Stream.of(2,4,5);
//        s1.forEach(System.out::println);


//        Supplier<Integer> supp = () -> new Random().nextInt();
//        Stream<Integer> s1 = Stream.generate(supp);
//        s1.limit(5)
//                .forEach(System.out::println);


//        Stream<Integer> s1 = Stream.iterate(1, i -> i + 1);
//        s1.limit(6)
//                .forEach(System.out::println);


        Stream<Integer> s = Stream.iterate(1,i -> i<=4,i -> i+1);
        s.forEach(System.out::println);

    }
}
