package org.example.OCP17.Stream.Lectia1.HomeWork;

import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {

        Stream<Integer> s = Stream.iterate(1, i -> i + 1);
        s.limit(4)
                .forEach(System.out::println);

        System.out.println("--");

        Stream<Integer> s1 = Stream.iterate(1, i -> i <= 6, i -> i + 1);
        s1.forEach(System.out::println);


    }
}
