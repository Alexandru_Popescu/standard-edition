package org.example.OCP17.Stream.Lectia1.TerminalOperator;

import java.util.List;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {

        List<Integer> list = List.of(1, 4, 5, 7, 9);
        boolean b1 = list.stream().anyMatch((x -> x % 2 == 0)); // cel putin una  //true
        boolean b2 = list.stream().allMatch((x -> x % 2 == 0)); // toate true // false
        boolean b3 = list.stream().noneMatch((x -> x % 2 == 0)); // niciuna // false
//        System.out.println(b1);
//        System.out.println(b2);
//        System.out.println(b3);

        Stream<Integer> s1 = Stream.iterate(1, i -> i + 1);

        boolean b4 = s1.anyMatch((x -> x % 2 == 0));
        System.out.println(b4);
//        s1.forEach(System.out::println);

    }
}
