package org.example.OCP17.Stream.Lectia1.ex1;

import java.util.List;

public class Example1 {
    public static void main(String[] args) {

        List<Integer> list = List.of(1, 5, 6, 3, 8, 9);
        for (Integer integer : list) {
            if (integer % 2 == 0) {
//                System.out.println("This are even number " + integer);
            }
        }

        list.stream()
                .filter(x -> x %2 ==0)
                .forEach(System.out::println);


    }
}
