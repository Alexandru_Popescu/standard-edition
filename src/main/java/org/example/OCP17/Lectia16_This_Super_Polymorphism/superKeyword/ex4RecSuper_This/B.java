package org.example.OCP17.Lectia16_This_Super_Polymorphism.superKeyword.ex4RecSuper_This;

public class B extends A {

    public B(){
        super(20);
        System.out.println("B1 " );
    }

    public B(int y){
        this();
        System.out.println("B2 " + y);
    }


}
