package org.example.OCP17.Lectia16_This_Super_Polymorphism.superKeyword.ex3;

public class A {

    int x;

    public A(int x){
        System.out.println("A" + x);
    }

}
