package org.example.OCP17.Lectia16_This_Super_Polymorphism.Recap;

public class Cat extends Animal {
    String catFoodPreference;

    public Cat(String catFoodPreference) {
        super(10, "Tom");
        this.catFoodPreference = catFoodPreference;
    }

    @Override
    public void makeNoise() {
        super.makeNoise();
        System.out.println("Meow meow");
        eat();
    }

}
