package org.example.OCP17.Lectia16_This_Super_Polymorphism.Recap;

public class Animal {

    int age;
    String name;

    public Animal(){}

    public Animal(int age, String name) {
        this.age = age;
        this.name = name;
    }

    public void makeNoise(){
        System.out.println("Hello, I'm an animal");
    }
    public void eat(){
        System.out.println("Munch munch");
    }

}
