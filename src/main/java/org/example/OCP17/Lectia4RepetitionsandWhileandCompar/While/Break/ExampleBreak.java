package org.example.OCP17.Lectia4RepetitionsandWhileandCompar.While.Break;

public class ExampleBreak {
    public static void main(String[] args) {
        int i = 1;

        while (true) {
            System.out.println(i);
            i++;
            if (i == 11) {
                break;
            }

        }


    }
}