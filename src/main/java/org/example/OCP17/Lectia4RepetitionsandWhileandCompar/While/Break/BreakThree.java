package org.example.OCP17.Lectia4RepetitionsandWhileandCompar.While.Break;

public class BreakThree {
    public static void main(String[] args) {
        int i = 1;

        while (true) {
            if (i == 11) {
                break;
            }
            System.out.println(i);
            i++;

        }


    }
}
