package org.example.OCP17.Lectia4RepetitionsandWhileandCompar.While;

public class Ex1 {
    public static void main(String[] args) {

        int i = 1;

        while (i <= 10) {
            System.out.println(i);
            i++;
        }
        System.out.println("The end");

    }
}
