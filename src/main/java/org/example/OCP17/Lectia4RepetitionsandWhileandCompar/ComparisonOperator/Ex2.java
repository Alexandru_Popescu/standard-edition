package org.example.OCP17.Lectia4RepetitionsandWhileandCompar.ComparisonOperator;

public class Ex2 {
    public static void main(String[] args) {

        Cat c1 = new Cat("Tom", 10);
        Cat c2 = new Cat("Tom", 10);

        c1 =c2;
        boolean result = c1 == c2;
        System.out.println(result);



    }
}


class Cat {

    String name;
    int age;

    public Cat(String name, int age) {
        this.name = name;
        this.age = age;
    }
}
