package org.example.OCP17.Lectia12AnonymousBlocks.Overloading;

public class A {

    /*  Overloading
     * by number
     * by type of at least one of them
     * order of types
     */

    //Example 1

    void a(int x) {
        System.out.println(x);
    }
    void a(Integer x){}
//    void a(double x){}


// Example 2:

    void a(int x, double y) {
    }

//    void a(int y, double x) {} -> atentie trb invers tipul, nu identificatorul

    void a(double x, int y) {
    }

    @Override
    public String toString() {
        return "true";
    }

    //Exemplu 3 Object with null


    void a(String x) {
    }

    void a(A a){};



}
