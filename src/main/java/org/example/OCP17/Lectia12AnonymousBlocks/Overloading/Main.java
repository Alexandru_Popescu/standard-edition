package org.example.OCP17.Lectia12AnonymousBlocks.Overloading;

public class Main {
    public static void main(String[] args) {

        A a1 = new A();
        a1.a((int) 10.4);

        // nu stie pe care sa o aleaga. Noi punem null la obiecte
        a1.a((String) null);


    }
}
