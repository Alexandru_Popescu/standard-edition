package org.example.OCP17.Lectia12AnonymousBlocks.Annonymous;

public class Cat {
    String name;
    static String test = ":)";
    final static String check;

    static {
        System.out.println(test);

    }

    Cat() {
        System.out.println("A cat is created");
    }

    {
        System.out.println("A");
    }

    static {
        System.out.println("b");
    }

    static void sayMeow() {
        System.out.println("Meow");
    }
    static {
        check="good";
        System.out.println(check);

    }

}
