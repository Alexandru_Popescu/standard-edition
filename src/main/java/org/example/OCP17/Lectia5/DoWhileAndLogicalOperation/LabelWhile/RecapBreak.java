package org.example.OCP17.Lectia5.DoWhileAndLogicalOperation.LabelWhile;

public class RecapBreak {
    public static void main(String[] args) {

        int i = 1;
        int j = 1;

        A:
        while (i <= 3) {
            B:
            while (j < 3) {
                System.out.println(":)");
                if (j % 2 == 0)
                    break B;
                j++;
            }
            i++;

        }


    }
}
