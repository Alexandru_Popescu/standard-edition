package org.example.OCP17.Lectia5.DoWhileAndLogicalOperation.LogicalOperator.Not;

public class Main {
    public static void main(String[] args) {

        boolean b = true;
        boolean res = !b;
        System.out.println(res);

        int x = 4;
        int y = 5;
        boolean res2 = !(x <y);
        System.out.println(res2);

    }
}
