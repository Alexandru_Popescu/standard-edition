package org.example.OCP17.Lectia5.DoWhileAndLogicalOperation.LabelWhile;

public class ExampleBreakTRee {
    public static void main(String[] args) {

        int i = 1;
        int j = 1;

        a: while (i <= 3) {
            b:while (j < 3) {
                System.out.println(":)");
                if (j % 2 == 0) {
                    break b;
                }
                j++;
            }
            i++;


        }
// break b ma scoate din innerLoop
    }
}
