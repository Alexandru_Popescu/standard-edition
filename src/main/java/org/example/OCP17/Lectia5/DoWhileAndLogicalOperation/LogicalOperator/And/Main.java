package org.example.OCP17.Lectia5.DoWhileAndLogicalOperation.LogicalOperator.And;

public class Main {
    public static void main(String[] args) {

    // &&

        // T-T -> T
        // F-T -> F
        // T-F -> F
        // F-F -> F

        boolean b1 = true;
        boolean b2 = false;
        boolean res = b1 & b2;
        System.out.println(res);

    }
}
