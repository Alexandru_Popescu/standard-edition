package org.example.OCP17.Lectia5.DoWhileAndLogicalOperation.DoWhile;

public class Ex1 {
    public static void main(String[] args) {
        int i = 1;

        do {
            System.out.println(i);
            i++;
        }while (i <10);
    }
}
