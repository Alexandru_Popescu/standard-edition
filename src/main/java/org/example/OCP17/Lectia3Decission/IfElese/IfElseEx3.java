package org.example.OCP17.Lectia3Decission.IfElese;

public class IfElseEx3 {
    public static void main(String[] args) {

        int x = 10;
        int y = 20;
        int z = 30;

        if (x > y) {
            if (x == z) {
                System.out.println("a");
            } else System.out.println("b");
        }else {

            System.out.println("ok");
        }
    }
}
