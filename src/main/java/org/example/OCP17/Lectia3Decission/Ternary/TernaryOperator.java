package org.example.OCP17.Lectia3Decission.Ternary;

public class TernaryOperator {
    public static void main(String[] args) {
        //Example1
//        int x  = 10;
//        int y = x >8 ? x : 1;
//        System.out.println(y);


        //Example2 -> Nested Ternary Operator

        int x = 5;
        int y = 7;
        int z = 9;

        int result = x < y ? (z > y ? 11 : 40) : 80;
        System.out.println(result);


    }
}
