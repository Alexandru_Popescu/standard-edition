package org.example.OCP17.Lectia3Decission.Switch;

public class Ex2 {
    public static void main(String[] args) {


        int x = 12;


        int y = switch (x) {

            case 10 -> 100;
            case 2 -> 200;
            default -> 1;

        };
        System.out.println(y);


    }
}
