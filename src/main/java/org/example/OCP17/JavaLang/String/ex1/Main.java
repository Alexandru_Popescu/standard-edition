package org.example.OCP17.JavaLang.String.ex1;

public class Main {
    public static void main(String[] args) {

        String s = "Hello";
        String s1 = "hello";

        System.out.println(s1.charAt(3));

        String s2 = new String("Hello");
        System.out.println(s == s1);

    }
}
