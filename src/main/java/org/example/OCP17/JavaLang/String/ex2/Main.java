package org.example.OCP17.JavaLang.String.ex2;

public class Main {
    public static void main(String[] args) {

        char s1 = "Hello".charAt(0);
        int s2 = "Hello".length();
        char c2 = "Hello".charAt(s2 - 1);
//        System.out.println(s2);

        int a = "ABC".compareTo("ABCDE");
        System.out.println(a);
    }
}
