package org.example.OCP17.Collection.Deque.EX;

import java.util.ArrayDeque;
import java.util.Deque;

public class Main {

    public static void main(String[] args) {

        Deque<Integer> d1 = new ArrayDeque<>();

        d1.push(3);
        d1.push(8);
        d1.push(9);
        d1.pop();

//        d1.addFirst(2);
//        d1.addFirst(3);
//        d1.addFirst(4);


        d1.forEach(System.out::println);

    }


}
