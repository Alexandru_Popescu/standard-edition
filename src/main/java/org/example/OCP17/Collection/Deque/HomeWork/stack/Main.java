package org.example.OCP17.Collection.Deque.HomeWork.stack;

import java.util.ArrayDeque;
import java.util.Deque;

public class Main {
    public static void main(String[] args) {

        Deque<Integer> d1 = new ArrayDeque<>();

        d1.offer(2);
        d1.offer(6);
        d1.offer(34);
        d1.offer(7657);
        d1.removeLast();
        //

        d1.forEach(System.out::println);


    }

}
