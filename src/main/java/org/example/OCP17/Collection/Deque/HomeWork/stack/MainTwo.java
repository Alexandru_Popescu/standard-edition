package org.example.OCP17.Collection.Deque.HomeWork.stack;

import java.util.ArrayDeque;
import java.util.Deque;

public class MainTwo {
    public static void main(String[] args) {

        Deque<Integer> d1 = new ArrayDeque<>();
        d1.push(34);
        d1.push(344);
        d1.push(343);
        d1.push(32344);
        d1.pop();
        d1.forEach(System.out::println);

    }
}
