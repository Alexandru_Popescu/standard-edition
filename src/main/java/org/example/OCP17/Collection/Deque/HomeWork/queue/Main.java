package org.example.OCP17.Collection.Deque.HomeWork.queue;

import java.util.ArrayDeque;
import java.util.Deque;

public class Main {
    public static void main(String[] args) {

        Deque<Integer> d1 = new ArrayDeque<>();

        d1.push(3);
        d1.push(5);
        d1.push(7);
        
        d1.forEach(System.out::println);

    }
}
