package org.example.OCP17.Collection.Deque.FinalExempleStack;

import java.util.ArrayDeque;
import java.util.Deque;

public class Main {

    public static void main(String[] args) {

        Deque<Integer> list1 = new ArrayDeque<>();
        list1.push(1);
        list1.push(2);
        list1.push(3);
        // example stack              LIFO             3 2 1
//        list1.pop();
//        list1.forEach(System.out::println);

        // queue  FIFO                   3 2
                 // 3 2 1
        list1.removeLast();
        list1.forEach(System.out::println);




    }


}
