package org.example.OCP17.Collection.Deque.EX;

import java.util.ArrayDeque;
import java.util.Deque;

public class MainTwo {
    public static void main(String[] args) {

        Deque<Integer> d1 = new ArrayDeque<>();
        d1.addLast(4);
        d1.addLast(5);
        d1.addLast(46);
        d1.addLast(2);


        // 4 5 46 2
        d1.forEach(System.out::println);


    }
}
