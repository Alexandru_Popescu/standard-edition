package org.example.OCP17.Collection.Deque.example;

import java.util.ArrayDeque;
import java.util.Deque;

public class Main {
    public static void main(String[] args) {

        Deque<Integer> list = new ArrayDeque<>();

        list.push(1);
        list.push(4);
        list.push(6);
//        list.forEach(System.out::println);
        //     6 4 1
//
//        list.removeFirst();
        list.pop();
        list.forEach(System.out::println);

        //  4 1




    }
}
