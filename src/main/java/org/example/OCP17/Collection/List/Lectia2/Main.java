package org.example.OCP17.Collection.List.Lectia2;

import java.util.ArrayList;
import java.util.List;



public class Main {
    public static void main(String[] args) {

        List<Integer> list = new ArrayList<>();

        list.add(10);
        list.add(20);
        list.add(34);
        list.add(1);

//        for (Integer integer : list) {
//            System.out.println(integer);
//        }
//
//        for (int i = 0; i< list.size(); i++){
//            System.out.println(list.get(i));
//        }

//       Iterator<Integer>listPrint = list.iterator();
//        while (listPrint.hasNext()){
//            int x= listPrint.next();
//            System.out.println(x);
//        }

        list.forEach(x -> System.out.println(x));





    }
}
