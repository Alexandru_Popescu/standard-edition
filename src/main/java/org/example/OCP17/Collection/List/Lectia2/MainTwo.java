package org.example.OCP17.Collection.List.Lectia2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MainTwo {

    public static void main(String[] args) {

        List<Integer> list = new ArrayList<>();
        list.add(3);
        list.add(2);
        list.add(45);


        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
        System.out.println("============");
        for (Integer i : list){
            System.out.println(i);
        }
        System.out.println("============");
        Iterator<Integer>iterator = list.iterator();

        while (iterator.hasNext()){
            int x = iterator.next();
            System.out.println(x);
        }

        System.out.println("======Lambda");
        list.forEach(x -> System.out.println(x));
        System.out.println("=dsdsd");
        list.remove((Integer) 3);
        System.out.println(list);

    }


}
