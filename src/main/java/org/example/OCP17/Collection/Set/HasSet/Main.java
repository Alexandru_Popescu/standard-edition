package org.example.OCP17.Collection.Set.HasSet;

import java.util.Set;

public class Main {
    public static void main(String[] args) {


        Set<Integer> set1 = Set.of(10,1000,23,300,256);
        for (Integer integer : set1) {
            System.out.println(integer);
        }

    }
}
