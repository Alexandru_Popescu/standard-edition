package org.example.OCP17.Collection.Set.TreeSet;

import java.util.Set;
import java.util.TreeSet;

public class Main {
    public static void main(String[] args) {

        Set<Integer> set = new TreeSet<>();
        set.add(10);
        set.add(1000);
        set.add(23);
        set.add(1000);
        set.add(300);
        set.add(256);
        for (Integer integer : set) {
            System.out.println(integer);
        }


    }
}
