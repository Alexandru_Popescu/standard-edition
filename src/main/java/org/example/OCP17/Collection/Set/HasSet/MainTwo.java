package org.example.OCP17.Collection.Set.HasSet;

import java.util.HashSet;
import java.util.Set;

public class MainTwo {
    public static void main(String[] args) {

        Set<Integer> set = new HashSet<>();
        set.add(10);
        set.add(1000);
        set.add(23);
        set.add(1000);
        set.add(300);
        set.add(256);
//        for (Integer integer : set) {
//            System.out.println(integer);
//        }


        set.forEach(x -> System.out.println(x));


    }
}
