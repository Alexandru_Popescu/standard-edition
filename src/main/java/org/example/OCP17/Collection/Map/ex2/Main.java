package org.example.OCP17.Collection.Map.ex2;

import java.util.TreeMap;

public class Main {

    public static void main(String[] args) {

        var a = new TreeMap<Cat, String>();

        a.put(new Cat(2),"Tom");
        a.put(new Cat(8),"Neo");
        System.out.println(a);
    }

}
