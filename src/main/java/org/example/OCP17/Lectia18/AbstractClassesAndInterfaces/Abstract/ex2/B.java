package org.example.OCP17.Lectia18.AbstractClassesAndInterfaces.Abstract.ex2;

abstract public class B extends A {

    @Override
    void m() {
        System.out.println("something");
    }

    abstract void n();
}
