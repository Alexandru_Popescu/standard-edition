package org.example.OCP17.Lectia18.AbstractClassesAndInterfaces.Interfaces.FactoryPatterrn;

public class Electric implements Engine {
    @Override
    public void run() {
        System.out.println("Electric");
    }
}
