package org.example.OCP17.Lectia18.AbstractClassesAndInterfaces.Interfaces.FactoryPatterrn;

public interface Engine {


    void run();


     static Engine build(String type) {

        return   switch (type) {
            case "electric " -> new Electric();
            case "diesel " -> new Diesel();
            default -> throw new IllegalArgumentException();


        };

    }





}
