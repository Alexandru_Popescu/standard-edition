package org.example.OCP17.Lectia18.AbstractClassesAndInterfaces.Interfaces.ex1;

public class Piano implements Playable {
//    @Override
//    public void play() {
//
//    }

    @Override
    public void m() {
        Playable.super.m();
    }

    @Override
    public void play() {
        System.out.println("Play! ");
    }
}
