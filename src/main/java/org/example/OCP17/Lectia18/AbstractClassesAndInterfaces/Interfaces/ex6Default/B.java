package org.example.OCP17.Lectia18.AbstractClassesAndInterfaces.Interfaces.ex6Default;

public interface B {

    default void m() {
        System.out.println("B");
    }
}
