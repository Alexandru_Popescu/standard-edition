package org.example.OCP17.Lectia18.AbstractClassesAndInterfaces.Interfaces.FactoryPatterrn;

public class Diesel implements Engine{
    @Override
    public void run() {
        System.out.println("Diesel");
    }
}
