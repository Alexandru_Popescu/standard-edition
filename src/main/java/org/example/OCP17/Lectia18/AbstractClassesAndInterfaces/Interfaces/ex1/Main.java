package org.example.OCP17.Lectia18.AbstractClassesAndInterfaces.Interfaces.ex1;

public class Main {
    public static void main(String[] args) {
        Playable p = new Piano();
        p.play();
    }
}
