package org.example.OCP17.Lectia18.AbstractClassesAndInterfaces.Interfaces.FactoryPatterrn;

public class Car {


    public Engine engine;

    public Car(Engine engine) {
        this.engine = engine;
    }

    public static Engine create(){
        return Engine.build("electric");
    }


}
