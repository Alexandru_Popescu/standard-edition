package org.example.OCP17.Lectia18.AbstractClassesAndInterfaces.Interfaces.FunctionalInterface;


@FunctionalInterface
public interface Playable {

    void play();
//    void n();

    default void m(){}
}
