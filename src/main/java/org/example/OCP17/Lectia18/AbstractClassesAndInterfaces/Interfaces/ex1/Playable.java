package org.example.OCP17.Lectia18.AbstractClassesAndInterfaces.Interfaces.ex1;

public interface Playable {

    int x = 10;

    void play();


//    private  void m(){
//        System.out.println();
//}

    default void m() {
        System.out.println(":)");
    }

  private   static void w() {
        System.out.println("}");
    }


    static void n() {


    }

}
