package org.example.OCP17.Lectia18.AbstractClassesAndInterfaces.Interfaces.FunctionalInterface;

public class Main {
    public static void main(String[] args) {


        Playable playable = () -> System.out.println("PLay!");

        playable.play();

    }
}