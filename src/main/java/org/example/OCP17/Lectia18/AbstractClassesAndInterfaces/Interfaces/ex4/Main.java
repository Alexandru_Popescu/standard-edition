package org.example.OCP17.Lectia18.AbstractClassesAndInterfaces.Interfaces.ex4;

public class Main {
    public static void main(String[] args) {
        B b1 = new B();
        if (b1 instanceof A){
            System.out.println("True");
        }else {
            System.out.println("False");
        }
    }
}
