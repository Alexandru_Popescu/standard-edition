package org.example.OCP17.Lectia18.AbstractClassesAndInterfaces.Interfaces.staticMethodsInterfaces;

public class Main {
    public static void main(String[] args) {

        A a1 = new B();
        a1.m2();

        A a = new A() {
            @Override
            public void m1() {
                A.super.m1();
            }
        };

    }
}
