package org.example.OCP17.Lectia18.AbstractClassesAndInterfaces.Interfaces.ex3;

public class A {

    private B b = new B();

    public void functionalityOfA() {

        // does here
        b.partOfWhatBHasToDo();

        //does here

    }

}
