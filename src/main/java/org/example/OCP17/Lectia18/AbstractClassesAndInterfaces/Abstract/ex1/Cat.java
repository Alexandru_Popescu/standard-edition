package org.example.OCP17.Lectia18.AbstractClassesAndInterfaces.Abstract.ex1;

public class Cat extends Pet {


    public Cat() {
        super("Tom");


    }

    public void sayMeow() {

        System.out.println("Meow! My name" + this.name);

    }


}
