package org.example.OCP17.Lectia18.AbstractClassesAndInterfaces.Abstract.ex3;

abstract public class A {

    public abstract void n();
    abstract public void m();

}
