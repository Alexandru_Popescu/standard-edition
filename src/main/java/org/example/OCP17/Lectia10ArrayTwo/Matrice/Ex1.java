package org.example.OCP17.Lectia10ArrayTwo.Matrice;

public class Ex1 {
    public static void main(String[] args) {

        int[][] y = new int[3][];
        y[0] = new int[]{1, 2, 3};
        y[1] = new int[]{2, 5};
        y[2] = new int[]{1};
        System.out.println("Y: " + y.length);

        for (int z = 0; z < y.length; z++) {
            System.out.println("Y[z]:" + y[z].length);
        }

        for (int[] a : y) {
            for (int b : a) {
                System.out.print(b);
            }
            System.out.println();

        }

        System.out.println("-------");

        int[][] x = {{1, 2, 3}, {2, 5}, {1}};
        System.out.println("X: " + x.length);


        for (int i = 0; i < x.length; i++) {
            System.out.println("X[i]:" + x[i].length);
        }

        for (int[] a : x) {
            for (int b : a) {
                System.out.print(b);
            }
            System.out.println();

        }

    }
}
