package org.example.OCP17.Lectia10ArrayTwo.Matrice;

public class Ex2 {
    public static void main(String[] args) {

        int[][] x = {{1, 2, 3}, {4, 5}, {6}};

        for (int[] y : x) {
            for (int z : y) {
                System.out.print(z + " ");
            }
            System.out.println();
        }


    }
}
