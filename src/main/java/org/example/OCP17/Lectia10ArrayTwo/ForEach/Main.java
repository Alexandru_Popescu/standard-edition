package org.example.OCP17.Lectia10ArrayTwo.ForEach;

public class Main {
    public static void main(String[] args) {

        var x = new int[]{1, 2, 3, 4, 5};

        for (int i = 0; i < x.length; i++) {
            x[i] = 10;
            System.out.println(x[i]);
        }
        System.out.println("================");
        for (int y : x) {
            y = 10;
// x =3;
            System.out.println(y);
        }
        System.out.println("=============");
        for (int z = 0; z < x.length; z++) {
            System.out.println(x[z]);
        }

    }
}
