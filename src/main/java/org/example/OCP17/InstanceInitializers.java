package org.example.OCP17;

public class InstanceInitializers {

    private String name = "Papy";

    {
        System.out.println("Something");
    }

    public InstanceInitializers() {
        name = "Alt";
        System.out.println("Another");
    }

    public static void main(String[] args) {
        InstanceInitializers instanceInitializers = new InstanceInitializers();
        System.out.println(instanceInitializers.name);
    }

}
