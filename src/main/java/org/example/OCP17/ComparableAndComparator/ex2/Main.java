package org.example.OCP17.ComparableAndComparator.ex2;

import java.util.Comparator;
import java.util.TreeSet;

public class Main {
    public static void main(String[] args) {

        Comparator<Dog> d = (d1, d2) -> d1.getAge() - d2.getAge();
        Comparator<Dog> dd = (d1, d2) -> d1.getName().compareTo(d2.getName());


        var set = new TreeSet<Dog>(d);

        set.add(new Dog("Z", 3));
        set.add(new Dog("C", 31));
        set.add(new Dog("B", 32));
        set.add(new Dog("D", 33));
        set.add(new Dog("A", 34));


        set.forEach(System.out::println);


    }
}
