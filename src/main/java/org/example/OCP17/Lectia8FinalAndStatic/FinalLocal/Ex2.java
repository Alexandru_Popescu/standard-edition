package org.example.OCP17.Lectia8FinalAndStatic.FinalLocal;

public class Ex2 {
    public static void main(String[] args) {
        m(5);
        m(6);

    }

    static void m(final int x) {
//        x = 10;
        System.out.println(x);
    }

}
