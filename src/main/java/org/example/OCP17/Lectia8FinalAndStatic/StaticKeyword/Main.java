package org.example.OCP17.Lectia8FinalAndStatic.StaticKeyword;

public class Main {
    public static void main(String[] args) {

        Dog dog1 = new Dog();
        Dog dog2 = new Dog();
        dog1.surName = " Tom";
        dog2.surName = " ABC";
        Dog.name = "WWW";

        System.out.println(dog1.surName);
        System.out.println(dog2.surName);

        dog1.name = "EEE";
        System.out.println(Dog.name);

    }
}