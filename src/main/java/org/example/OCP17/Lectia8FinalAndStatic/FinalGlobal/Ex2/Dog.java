package org.example.OCP17.Lectia8FinalAndStatic.FinalGlobal.Ex2;

public class Dog {
    final String name;

    public Dog(String name ) {
        this.name = name ;
    }
}
