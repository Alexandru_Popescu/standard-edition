package org.example.OCP17.Lectia8FinalAndStatic.FinalGlobal.Ex1;

public class Main {
    public static void main(String[] args) {
        Cat c1 = new Cat();
        Cat c2 = new Cat();
        c1.name = "Leo";
        c2.name = "Tom";
        System.out.println(c1.name);
        System.out.println(c2.name);
        c1.name="Mitzi";
        System.out.println(c1.name);

    }
}
