package org.example.OCP17.Lectia9Array.Ex1;

public class Main {
    public static void main(String[] args) {

        int[] x = new int[5];
        x[0] = 8;
        x[1] = 9;
        x[2] = 7;
        x[3] = 1;
        x[4] = 2;

        for (int z = 0; z<x.length; z++) {
            System.out.println(x[z]);
        }
        System.out.println("======");

        for (int w : x){
            System.out.println(w);
        }

        }
    }

