package org.example.OCP17.Lectia9Array.Ex2Sintax;

public class Main {
    public static void main(String[] args) {
        // Example 1;
        int[] x;
        x = new int[4];
        // Example 2;
        int[] y = {};
        int[] b;

//        b = {3,5,6};

        // Example 3;
        int[] c;
        c = new int[]{3, 4, 5,};

        // Example 4;

        //    var d ={3,4,5,};
        var e = new int[10];

        var f = new int[]  {0,1,2,3};
    }
}
