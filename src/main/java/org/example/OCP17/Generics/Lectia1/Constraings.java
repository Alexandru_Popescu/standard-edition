package org.example.OCP17.Generics.Lectia1;

public class Constraings {
    public static void main(String[] args) {


        Foo<Integer> f1;
        Foo<?> f2;
        Foo<? extends Number> f3;
        Foo<? super Number> f4;

        f1 = new Foo<Integer>();
        f2 = new Foo<Double>();
        f3 = new Foo<Number>();
        f4 = new Foo<Object>();
        Foo<? extends Object> foo = new Foo<Number>();


        Foo<Number> f7 = new Foo<Number>();
        f7.x = 6;
        f7.x = 10;

        Foo<?> f8 = new Foo<Object>();
        //f8.x = 34; //

    }
}
