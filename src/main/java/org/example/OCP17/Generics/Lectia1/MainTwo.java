package org.example.OCP17.Generics.Lectia1;

public class MainTwo {
    public static void main(String[] args) {

        Foo<String> f3 = new Foo<>();
        f3.x="Hello";

        Foo<Integer> f4 = new Foo<>();
        f4.x=10;

    }
}
