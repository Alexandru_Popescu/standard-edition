package org.example.OCP17.Generics.Recap.Multiple;

public class A <T,G>{

    T a;
    G b;

    public A(T a) {
        this.a = a;
    }
}
