package org.example.OCP17.Generics.Recap.Boundaring;

public class Main {

    public static void main(String[] args) {

        A<?> a1 = new A<Integer>();
        A<Integer> a2 = new A<Integer>();
        a2.a = 34;

    }
}
