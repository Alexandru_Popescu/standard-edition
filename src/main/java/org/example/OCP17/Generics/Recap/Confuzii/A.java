package org.example.OCP17.Generics.Recap.Confuzii;

public class A <T>{

    T x;

    public A(T x) {
        this.x = x;
    }
}
