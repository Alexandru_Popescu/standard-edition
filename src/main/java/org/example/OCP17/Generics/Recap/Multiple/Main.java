package org.example.OCP17.Generics.Recap.Multiple;

public class Main {
    public static void main(String[] args) {

        A <?,Integer> a1 = new A<String,Integer>("45");
        a1.b=34;

        System.out.println(a1.a);
        System.out.println(a1.b);


    }
}
