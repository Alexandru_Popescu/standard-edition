package org.example.OCP17.Generics.Recap.Constrains;

public class Main {
    public static void main(String[] args) {


//        A a = new A();





        //1
        A<Integer> a1 = new A<Integer>();
        a1.a = 5;

        //2
        A<?> a2 = new A<Integer>();
//        a2.a=5;
        //3
        A<? extends Number> a3 = new A<Integer>();
//        a3.a=34;
        //4

        A<? super Number> a4 = new A<Object>();
        a4.a = 4;
    }
}
