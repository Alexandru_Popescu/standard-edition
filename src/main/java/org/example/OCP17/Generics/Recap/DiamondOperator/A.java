package org.example.OCP17.Generics.Recap.DiamondOperator;

public class A <T>{

    T a;

    public A(T a) {
        this.a = a;
    }
}
