package org.example.OCP17.Lectia6IncrementAndFor.For;

public class exThree {
    public static void main(String[] args) {

        for (int x = 0, y = 10; x < y; x++, y--) {
            System.out.println(x + y);
        }


/*
   10
   10
   10
   10
   10
 */

    }
}
