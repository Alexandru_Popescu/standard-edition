package org.example.OCP17.Lectia6IncrementAndFor.Increment;

public class IncrementThree {
    public static void main(String[] args) {

        int i = 0;
        while (i < 10) {
            i = i++;
            System.out.println(i);
        }
    }
}
