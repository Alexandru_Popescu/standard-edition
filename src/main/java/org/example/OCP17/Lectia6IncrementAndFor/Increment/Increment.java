package org.example.OCP17.Lectia6IncrementAndFor.Increment;

public class Increment {
    public static void main(String[] args) {

        int x = 10;
        int y = 20;
        int z = x++ + ++x; //  10 + 12

        System.out.println(x);   // x =12
        System.out.println(y);   // y = 20
        System.out.println(z);   // z = 22


    }
}
