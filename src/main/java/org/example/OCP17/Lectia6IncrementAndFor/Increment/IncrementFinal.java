package org.example.OCP17.Lectia6IncrementAndFor.Increment;

public class IncrementFinal {
    public static void main(String[] args) {

//        int i = 1;
//        Exercise 1:
//        System.out.println(i++);
//        System.out.println(++i);

        //Exercise 2:

//        i++;
//        System.out.println(i);
//        ++i;
//        System.out.println(i);


//        Exercise 3:

//        i++;
//        System.out.println(i);
//        ++i;
//        System.out.println(i);


        //Exercise 4;


//        int x = 10;
//        int y = 25;
//        int z = 45;
//        int w = 20;
//
//        x = ++x + ++x + x++ + ++y - y++  + z++ + z++  + w++ + w++ + ++w;
//
//        // 11 + 12 + 12 + 26 -26 + 45 +46 + 20 +21 +23
//        // x =35 , y=53  z 93    w =66;
//        System.out.println(x);


        //Exercise 5;


        int x = 5;
        int y = 10;
        int z = 20;
        int w = 30;

        x = ++x + x++ + x++ + ++x +   ++y + ++y +   ++w + w++ + w++ + ++w +  z++ + ++z + ++z + z++ + z++ + ++z;
        //  6   + 6 +   7 + 9 +        11 + 12 +     31+  31 +  32 + 34    +  20 + 22  + 23 + 23 + 24 +26

        System.out.println(x);

    }
}
