package org.example.OCP17.Lectia6IncrementAndFor.Increment;

public class IncrementFive {
    public static void main(String[] args) {

        int a = 4;
        int b = 2;
        int d = 6;
        int e = 8;

        int result = a++ + a++ + ++a + ++e + ++d + ++e + ++b + a++ + b++ + b++ +a++ + e++ + ++e;
 //                  4   +  5  +   7  +  9    7 +  10  +  3  + 7 +   3   + 4    8    10    12


        System.out.println(result);
        System.out.println(a);
        System.out.println(a + 1);
        System.out.println(a);

  // i = i++  ==/   i = i+1
    }
}
